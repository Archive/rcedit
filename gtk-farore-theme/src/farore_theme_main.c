#include "thinice_theme.h"
#include <gmodule.h>

/* Theme functions to export */
void                theme_init(GtkThemeEngine * engine);
void                theme_exit(void);

/* Exported vtable from th_draw */

extern GtkStyleClass thinice_default_class;

/* internals */

/* external theme functions called */

static struct
  {
    gchar       *name;
    guint        token;
  }
theme_symbols[] =
{
  { "rect_scrollbar",      TOKEN_RECTSCROLLBAR },
  { "scrollbar_marks",     TOKEN_SCROLLBARMARKS },
  { "scroll_button_marks", TOKEN_SCROLLBUTTONMARKS },
  { "handlebox_marks",     TOKEN_HANDLEBOXMARKS },
  
  { "TRUE",                TOKEN_TRUE },
  { "FALSE",               TOKEN_FALSE },
};

static guint n_theme_symbols = sizeof(theme_symbols) / sizeof(theme_symbols[0]);

static guint
theme_parse_scrollmarks(GScanner *scanner,
                        ThemeRcData *theme_data)
{
  guint               token, which_token;
  guint               set_to;
  
  which_token = g_scanner_get_next_token(scanner);
  if (!((which_token == TOKEN_SCROLLBARMARKS) ||
        (which_token == TOKEN_SCROLLBUTTONMARKS)))
    return TOKEN_SCROLLBARMARKS;
  
  token = g_scanner_get_next_token(scanner);
  if (token != G_TOKEN_EQUAL_SIGN)
    return G_TOKEN_EQUAL_SIGN;
  
  token = g_scanner_get_next_token(scanner);
  if (token == TOKEN_TRUE)
    set_to = MARKS_ON;
  else if (token == TOKEN_FALSE)
    set_to = MARKS_OFF;
  else
    return TOKEN_TRUE;

  switch (which_token) {
    case TOKEN_SCROLLBARMARKS:
      theme_data->scrollbar_marks = set_to;
      break;
    case TOKEN_SCROLLBUTTONMARKS:
    default:
      theme_data->scroll_button_marks = set_to;
      break;
  }

  return G_TOKEN_NONE;
}

static guint
theme_parse_scrollbar(GScanner *scanner,
                      ThemeRcData *theme_data)
{
  guint               token;
  
  token = g_scanner_get_next_token(scanner);
  if (token != TOKEN_RECTSCROLLBAR)
    return TOKEN_RECTSCROLLBAR;
  
  token = g_scanner_get_next_token(scanner);
  if (token != G_TOKEN_EQUAL_SIGN)
    return G_TOKEN_EQUAL_SIGN;
  
  token = g_scanner_get_next_token(scanner);
  if (token == TOKEN_TRUE)
    theme_data->scrollbar_type = SCROLL_RECT;
  else if (token == TOKEN_FALSE)
    theme_data->scrollbar_type = SCROLL_SHAPED;
  else
    return TOKEN_TRUE;

  return G_TOKEN_NONE;
}

static guint
theme_parse_handleboxmarks(GScanner *scanner,
                           ThemeRcData *theme_data)
{
  guint               token;
  
  token = g_scanner_get_next_token(scanner);
  if (token != TOKEN_HANDLEBOXMARKS)
    return TOKEN_HANDLEBOXMARKS;
  
  token = g_scanner_get_next_token(scanner);
  if (token != G_TOKEN_EQUAL_SIGN)
    return G_TOKEN_EQUAL_SIGN;
  
  token = g_scanner_get_next_token(scanner);
  if (token == TOKEN_TRUE)
    theme_data->handlebox_marks = MARKS_ON;
  else if (token == TOKEN_FALSE)
    theme_data->handlebox_marks = MARKS_OFF;
  else
    return TOKEN_TRUE;

  return G_TOKEN_NONE;
}

guint
theme_parse_rc_style(GScanner * scanner,
		     GtkRcStyle * rc_style)
{
  static GQuark       scope_id = 0;
  ThemeRcData        *theme_data;
  guint               old_scope;
  guint               token;
  guint               i;

  /* Set up a new scope in this scanner. */

  if (!scope_id)
    scope_id = g_quark_from_string("theme_engine");

  /* If we bail out due to errors, we *don't* reset the scope, so the
   * error messaging code can make sense of our tokens.
   */
  old_scope = g_scanner_set_scope(scanner, scope_id);
  
  /* Now check if we already added our symbols to this scope
   * (in some previous call to theme_parse_rc_style for the
   * same scanner.
   */

  if (!g_scanner_lookup_symbol(scanner, theme_symbols[0].name))
    {
      g_scanner_freeze_symbol_table(scanner);
      for (i = 0; i < n_theme_symbols; i++)
        {
          g_scanner_scope_add_symbol(scanner, scope_id,
              theme_symbols[i].name,
              GINT_TO_POINTER(theme_symbols[i].token));
        }
      g_scanner_thaw_symbol_table(scanner);
    }

  /* We're ready to go, now parse the top level */

  theme_data = g_new(ThemeRcData, 1);
  theme_data->scrollbar_type = DEFAULT_SCROLLSHAPE;
  theme_data->scrollbar_marks = DEFAULT_SCROLLBARMARKS;
  theme_data->scroll_button_marks = DEFAULT_SCROLLBUTTONMARKS;
  theme_data->handlebox_marks = DEFAULT_HANDLEBOXMARKS;

  token = g_scanner_peek_next_token(scanner);
  while (token != G_TOKEN_RIGHT_CURLY)
    {
      switch (token)
	{
        case TOKEN_RECTSCROLLBAR:
          token = theme_parse_scrollbar(scanner, theme_data);
          break;
        case TOKEN_SCROLLBUTTONMARKS:
        case TOKEN_SCROLLBARMARKS:
          token = theme_parse_scrollmarks(scanner, theme_data);
          break;
        case TOKEN_HANDLEBOXMARKS:
          token = theme_parse_handleboxmarks(scanner, theme_data);
          break;
	default:
	  g_scanner_get_next_token(scanner);
	  token = G_TOKEN_RIGHT_CURLY;
	  break;
	}

      if (token != G_TOKEN_NONE)
	{
	  g_free(theme_data);
	  return token;
	}
      token = g_scanner_peek_next_token(scanner);
    }

  g_scanner_get_next_token(scanner);

  rc_style->engine_data = theme_data;
  g_scanner_set_scope(scanner, old_scope);

  return G_TOKEN_NONE;
}

void
theme_merge_rc_style(GtkRcStyle * dest,
		     GtkRcStyle * src)
{
  ThemeRcData        *src_data = src->engine_data;
  ThemeRcData        *dest_data = dest->engine_data;

  if (!dest_data)
    {
      dest_data = g_new(ThemeRcData, 1);
      dest->engine_data = dest_data;
    }

  dest_data->scrollbar_type = src_data->scrollbar_type;
  dest_data->scrollbar_marks = src_data->scrollbar_marks;
  dest_data->scroll_button_marks = src_data->scroll_button_marks;
  dest_data->handlebox_marks = src_data->handlebox_marks;
}

void
theme_rc_style_to_style(GtkStyle * style,
			GtkRcStyle * rc_style)
{
  ThemeRcData        *data = rc_style->engine_data;
  ThemeStyleData     *style_data;

  style_data = g_new(ThemeStyleData, 1);
  style_data->scrollbar_type = data->scrollbar_type;
  style_data->scrollbar_marks = data->scrollbar_marks;
  style_data->scroll_button_marks = data->scroll_button_marks;
  style_data->handlebox_marks = data->handlebox_marks;

  style->klass = &thinice_default_class;
  style->engine_data = style_data;
}

void
theme_duplicate_style(GtkStyle * dest,
		      GtkStyle * src)
{
  ThemeStyleData     *dest_data;
  ThemeStyleData     *src_data = src->engine_data;

  dest_data = g_new(ThemeStyleData, 1);
  dest_data->scrollbar_type = src_data->scrollbar_type;
  dest_data->scrollbar_marks = src_data->scrollbar_marks;
  dest_data->scroll_button_marks = src_data->scroll_button_marks;
  dest_data->handlebox_marks = src_data->handlebox_marks;

  dest->klass = &thinice_default_class;
  dest->engine_data = dest_data;
}

void
theme_realize_style(GtkStyle * style)
{
}

void
theme_unrealize_style(GtkStyle * style)
{
}

void
theme_destroy_rc_style(GtkRcStyle * rc_style)
{
  ThemeRcData        *data = rc_style->engine_data;

  if (data)
    {
      g_free(data);
    }
}

void
theme_destroy_style(GtkStyle * style)
{
  ThemeStyleData     *data = style->engine_data;

  if (data)
    {
      g_free(data);
    }
}

void
theme_set_background(GtkStyle * style,
		     GdkWindow * window,
		     GtkStateType state_type)
{
  GdkPixmap          *pixmap;
  gint                parent_relative;

  g_return_if_fail(style != NULL);
  g_return_if_fail(window != NULL);

  if (style->bg_pixmap[state_type])
    {
      if (style->bg_pixmap[state_type] == (GdkPixmap *) GDK_PARENT_RELATIVE)
	{
	  pixmap = NULL;
	  parent_relative = TRUE;
	}
      else
	{
	  pixmap = style->bg_pixmap[state_type];
	  parent_relative = FALSE;
	}

      gdk_window_set_back_pixmap(window, pixmap, parent_relative);
    }
  else
    gdk_window_set_background(window, &style->bg[state_type]);
}

void
theme_init(GtkThemeEngine * engine)
{
    GtkRangeClass *rangeclass;

    engine->parse_rc_style = theme_parse_rc_style;
    engine->merge_rc_style = theme_merge_rc_style;
    engine->rc_style_to_style = theme_rc_style_to_style;
    engine->duplicate_style = theme_duplicate_style;
    engine->realize_style = theme_realize_style;
    engine->unrealize_style = theme_unrealize_style;
    engine->destroy_rc_style = theme_destroy_rc_style;
    engine->destroy_style = theme_destroy_style;
    engine->set_background = theme_set_background;
    
    rangeclass = (GtkRangeClass *)gtk_type_class(gtk_range_get_type());
    rangeclass->slider_width = DEFAULT_SLIDER_WIDTH;
    rangeclass->stepper_size = DEFAULT_SLIDER_WIDTH;
    rangeclass->min_slider_size = DEFAULT_MIN_SLIDER_SIZE;
}

void
theme_exit(void)
{
  printf("ThinIce Theme Exit\n* Need to add memory deallocation code here *\n");
}

/* The following function will be called by GTK+ when the module
 * is loaded and checks to see if we are compatible with the
 * version of GTK+ that loads us.
*/
G_MODULE_EXPORT const gchar* g_module_check_init (GModule *module);

const gchar*
g_module_check_init (GModule *module)
{
	return gtk_check_version(GTK_MAJOR_VERSION,
				 GTK_MINOR_VERSION,
				 GTK_MICRO_VERSION - GTK_INTERFACE_AGE);
}
