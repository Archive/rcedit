#ifndef __GRADIENT_H__
#define __GRADIENT_H__

#include <gdk-pixbuf/gdk-pixbuf.h>
#include <gdk/gdk.h>

/* Gradient effects. */

typedef enum {
	VERTICAL, HORIZONTAL, DIAGONAL, CROSSDIAGONAL,
	PYRAMID, RECTANGLE, PIPECROSS, ELLIPTIC
} GradientType;

void draw_gradient (GdkPixBuf *image, GdkColor *begin, GdkColor *end,
		    GradientType type, guint sx, guint sy, guint ex, guint ey);
/*void draw_blend (GdkPixBuf *image, float initial_intensity, GdkColor *bgnd,
		 GradientType type, gboolean anti_dir, guint sx, guint sy,
		 guint ex, guint ey);*/

#endif /* __GRADIENT_H__ */
