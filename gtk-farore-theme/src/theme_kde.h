#ifndef __FARORE_THEME_H__
#define __FARORE_THEME_H__

#include "gradient.h"

/* Yes mosfet, I copied your enum. Nyah nyah boo-boo */
typedef enum {
	PushButton=0, ComboBox, HScrollBarSlider, VScrollBarSlider, Bevel,
	ToolButton, ScrollButton, HScrollDeco, VScrollDeco, ComboDeco,
	MenuItem, InactiveTab, ArrowUp, ArrowDown, ArrowLeft, ArrowRight,
	
	PushButtonDown, ComboBoxDown, HScrollBarSliderDown,
	VScrollBarSliderDown, BevelDown, ToolButtonDown, ScrollButtonDown,
	HScrollDecoDown, VScrollDecoDown, ComboDecoDown, MenuItemDown,
	ActiveTab, SunkenArrowUp, SunkenArrowDown, SunkenArrowLeft,
	SunkenArrowRight,

	HScrollGroove, VScrollGroove, Slider, SliderGroove, IndicatorOn,
	IndicatorOff, ExIndicatorOn, ExIndicatorOff, HBarHandle, VBarHandle,
	ToolBar, Splitter, CheckMark, MenuBar, DisArrowUp, DisArrowDown,
	DisArrowLeft, DisArrowRight, ProgressBar, ProgressBg, MenuBarItem,
	Background,
	
	WIDGET_TYPES
} WidgetType;

typedef enum {
	HORIZONTAL, VERTICAL, FULL, TILE
} ScaleType;

typedef struct {
	gint left, right;
	gint top, bottom;
} Border;

typedef struct {
	guint refcount;
	
	/* General properties */
	enum { RASTER, PIXMAP, STYLE, GRADIENT, BLEND } type;
	ScaleType scale; 
	gchar *file; /* PIXMAP, BLEND, or RASTER */
	Border border;
	
	/* General identifiers */
	
	/* KDE stuff */	
	WidgetType widget;
	GdkColor *begin; /* GRADIENT or BLEND */
	GdkColor *end;  /* GRADIENT */
	GradientType grad;
	guint style; /* Only for STYLE */
	gchar *border_file; /* Pixmap for border */
	
	/* Raster's stuff */
	gboolean recolorable;
	gchar *overlay_file;
	Border overlay_border;
} WidgetInfo;
		
void theme_kde_init (char *themefile);

void draw_pixmap (WidgetInfo *info, guint w, guint h, GtkWidget *widget,
                       GdkGC *gc, GdkWindow *window);

#endif /* __FARORE_THEME_H__ */
