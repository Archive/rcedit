#include <math.h>
#include "gradient.h"

#define l_pix(x, y) (pix->pixels[pix->width * (y) + (x)])

void
swap (int *x, int *y)
{
	int tmp;
	
	g_return_if_fail (x != NULL);
	g_return_if_fail (y != NULL);
	
	tmp = *x;
	*x = *y;
	*y = tmp;
}
					
void draw_gradient (GdkPixBuf *image, GdkColor *begin, GdkColor *end,
		    GradientType type, guint sx, guint sy, guint ex, guint ey)
{
	double dxr, dxg, dxb, dyr, dyg, dyb;
	double dr, dg, db;
	double rx, gx, bx, ry, gy, by;
	double w, h;
	int x, y;
	int inc = 1;
	ArtPixBuf *pix;
	art_u8 *p;
	
	g_return_if_fail (image != NULL);
	g_return_if_fail (begin != NULL);
	g_return_if_fail (end != NULL);
	g_return_if_fail (ex > sx);
	g_return_if_fail (ey > sy);
	g_return_if_fail (image->art_pixbuf != NULL);
	g_return_if_fail (image->art_pixbuf->width > ex);
	g_return_if_fail (image->art_pixbuf->height > ey);
	
	pix = image->art_pixbuf;

	if (type == VERTICAL) {
		swap (&sx, &sy);
		swap (&ex, &ey);
	} 
	w = ex - sx;
	h = ey - sy;
	
	dr = end->red - begin->red;
	dg = end->green - begin->green;
	db = end->blue - begin->blue;
	
	dxr = dr / w;
	dyr = dr / h;
	dxg = dg / w;
	dyg = dg / h;
	dxb = db / w;
	dyb = db / h;
	
	rx = ry = begin->red;
	gx = gy = begin->green;
	bx = by = begin->blue;
		 
	if (type == VERTICAL || type == HORIZONTAL) {
		for (y = sy; y <= ey; y += inc) {
			rx = ry = begin->red;
			gx = gy = begin->green;
			bx = by = begin->blue;

			for (x = sx; x <= ex; x += inc) {
				if (type == VERTICAL)
					p = &(l_pix (y * 3, x * 3));
				else
					p = &(l_pix (x * 3, y * 3));

				*p++ = rx;
				*p++ = gx;
				*p++ = bx;

				rx += dxr;
				gx += dxg;
				bx += dxb;
			}
		}
	} else {
		if (type == DIAGONAL || type == CROSSDIAGONAL) {
			int width = (int) w + 1;
			int height = (int) h + 1;
			int i, j;
			art_u8 xtable[3][width], ytable[3][height];
			double ix = 1/(int)w, iy = 1/(int)h;
		
			for (x = 0; x < width; x++) {
				xtable[0][x] = rx;
				xtable[1][x] = gx;
				xtable[2][x] = bx;
				
				rx += (dxr+ix)/2;
				gx += (dxg+ix)/2;
				bx += (dxb+ix)/2;
			}
			
			for (y = 0; y < height; y++) {
				ytable[0][y] = ry;
				ytable[1][y] = gy;
				ytable[2][y] = by;
				
				ry += (dyr+iy)/2;
				gy += (dyg+iy)/2;
				by += (dyb+iy)/2;
			}
			
			if (type == CROSSDIAGONAL) {
				swap (&sx, &ex);
				swap (&sy, &ey);
				inc = -1;
			}
			j = 0;
			for (y = sy; ((type == CROSSDIAGONAL && y >= ey) || (type == DIAGONAL && y <= ey)) && j < height; y += inc) {
				i = 0;
				for (x = sx; ((type == CROSSDIAGONAL && x >= ex) || (type == DIAGONAL && x <= ex)) && i<width; x += inc) {
					p = &(l_pix ((x) * 3, (y) * 3));

					*p++ = ytable[0][j] + xtable[0][i];
					*p++ = ytable[1][j] + xtable[1][i];
					*p++ = ytable[2][j] + xtable[2][i];
					i++;
				}
				j++;
			}
		} else if (type == PYRAMID || type == RECTANGLE || type == PIPECROSS || type == ELLIPTIC) {
			int width = (int)w+1;
			int height = (int)h+1;
			int i, j;
			art_u8 xtable[3][width], ytable[3][height];
			art_u8 *p2, *p3, *p4;
		
			for (x = 0; x < width; x++) {
				xtable[0][x] = rx;
				xtable[1][x] = gx;
				xtable[2][x] = bx;
				
				rx += dxr;
				gx += dxg;
				bx += dxb;
			}
			
			for (y = 0; y < height; y++) {
				ytable[0][y] = ry;
				ytable[1][y] = gy;
				ytable[2][y] = by;
				
				ry += dyr;
				gy += dyg;
				by += dyb;
			}
			
			for (y = sy, j = 0; y <= ey/2; y++, j++) {
				for (x = sx, i = 0; x <= ex/2; x++, i++) {
					p = &(l_pix ((x) * 3, (y) * 3));
					p2 = &(l_pix ((ex - i) * 3, (y) * 3));
					p3 = &(l_pix ((ex - i) * 3, (ey - j) * 3));
					p4 = &(l_pix ((x) * 3, (ey - j) * 3));

					*p++ = *p2++ = *p3++ = *p4++ = ytable[0][j] + xtable[0][i];
					*p++ = *p2++ = *p3++ = *p4++ = ytable[1][j] + xtable[1][i];
					*p++ = *p2++ = *p3++ = *p4++ = ytable[2][j] + xtable[2][i];

					
				}
			}
			
		} else return;
	} 
}
