#include "widgets.h"

static struct
  {
	  gchar *name;
	  WidgetType type;
  }
widget_keys[] =
{

	{
		"GtkArrow", W_GTK_ARROW
	}
	,
	{
		"GtkButton", W_GTK_BUTTON
	}
	,
	{
		"GtkCheckButton", W_GTK_CHECKBUTTON
	}
	,
	{
		"GtkCList", W_GTK_CLIST
	}
	,
	{
		"GtkCTree", W_GTK_CTREE
	}
	,
	{
		"GtkCurve", W_GTK_CURVE
	}
	,
	{
		"GtkDialog", W_GTK_DIALOG
	}
	,
	{
		"GtkDrawingArea", W_GTK_DRAWINGAREA
	}
	,
	{
		"GtkEditable", W_GTK_EDITABLE
	}
	,
	{
		"GtkEntry", W_GTK_ENTRY
	}
	,
	{
		"GtkEventBox", W_GTK_EVENTBOX
	}
	,
	{
		"GtkFileSelection", W_GTK_FILESEL
	}
	,
	{
		"GtkFontSelection", W_GTK_FONTSEL
	}
	,
	{
		"GtkHScale", W_GTK_HSCALE
	}
	,
	{
		"GtkHScrollbar", W_GTK_HSCROLL
	}
	,
	{
		"GtkItem", W_GTK_ITEM
	}
	,
	{
		"GtkList", W_GTK_LIST
	}
	,
	{
		"GtkListItem", W_GTK_LISTITEM
	}
	,
	{
		"GtkMenu", W_GTK_MENU
	}
	,
	{
		"GtkMenuBar", W_GTK_MENUBAR
	}
	,
	{
		"GtkMenuItem", W_GTK_MENUITEM
	}
	,
	{
		"GtkNotebook", W_GTK_NOTEBOOK
	}
	,
	{
		"GtkOptionMenu", W_GTK_OPTIONMENU
	}
	,
	{
		"GtkProgressBar", W_GTK_PROGRESSBAR
	}
	,
	{
		"GtkRadioButton", W_GTK_RADIOBUTTON
	}
	,
	{
		"GtkRange", W_GTK_RANGE
	}
	,
	{
		"GtkRuler", W_GTK_RULER
	}
	,
	{
		"GtkScale", W_GTK_SCALE
	}
	,
	{
		"GtkScrollbar", W_GTK_SCROLL
	}
	,
	{
		"GtkSpinButton", W_GTK_SPINBUTTON
	}
	,
	{
		"GtkText", W_GTK_TEXT
	}
	,
	{
		"GtkToggleButton", W_GTK_TOGGLEBUTTON
	}
	,
	{
		"GtkTree", W_GTK_TREE
	}
	,
	{
		"GtkTreeItem", W_GTK_TREEITEM
	}
	,
	{
		"GtkVScale", W_GTK_VSCALE
	}
	,
	{
		"GtkVScrollbar", W_GTK_VSCROLL
	}
	,
	{
		"GtkWidget", W_GTK_WIDGET
	}
	,
	{
		"GtkWindow", W_GTK_WINDOW
	}
};

void 
widgets_init (void)
{
	int i;
	int len = sizeof (widget_keys) / sizeof (widget_keys[0]);
	widget_hash = g_hash_table_new (g_str_hash, g_str_equal);
	g_hash_table_freeze (widget_hash);
	for (i = 0; i < len; i++)
		g_hash_table_insert (widget_hash, widget_keys[i].name,
				     GUINT_TO_POINTER (widget_keys[i].type));
	g_hash_table_thaw (widget_hash);
}

GtkWidget *
create_widget (WidgetType type)
{
	GtkWidget *widget = NULL;
	static gchar *titles[] =
	{_ ("One"), _ ("Two"), NULL};

	switch (type)
	  {
	  case W_GTK_ARROW:
		  widget = gtk_arrow_new (GTK_ARROW_UP, GTK_SHADOW_OUT);
		  break;
	  case W_GTK_BUTTON:
		  widget = gtk_button_new_with_label (_ ("Test"));
		  break;
	  case W_GTK_CHECKBUTTON:
		  widget = gtk_check_button_new ();
		  break;
	  case W_GTK_CLIST:
		  widget = gtk_clist_new_with_titles (2, titles);
		  break;
	  case W_GTK_CTREE:
		  widget = gtk_ctree_new_with_titles (2, 0, titles);
		  break;
	  case W_GTK_CURVE:
		  widget = gtk_curve_new ();
		  break;
	  case W_GTK_DIALOG:
		  widget = gtk_dialog_new ();
		  break;
	  case W_GTK_DRAWINGAREA:
		  widget = gtk_drawing_area_new ();
		  break;
	  case W_GTK_EDITABLE:
		  widget = gtk_entry_new ();
		  break;
	  case W_GTK_ENTRY:
		  widget = gtk_entry_new ();
		  break;
	  case W_GTK_EVENTBOX:
		  widget = gtk_event_box_new ();
		  break;
	  case W_GTK_FILESEL:
		  widget = gtk_file_selection_new (_ ("File Selection"));
		  break;
	  case W_GTK_FONTSEL:
		  widget = gtk_font_selection_new ();
		  break;
	  case W_GTK_HSCALE:
		  widget = gtk_hscale_new (NULL);
		  break;
	  case W_GTK_HSCROLL:
		  widget = gtk_hscrollbar_new (NULL);
		  break;
	  case W_GTK_ITEM:
		  widget = gtk_list_item_new ();
		  break;
	  case W_GTK_LIST:
		  widget = gtk_list_new ();
		  break;
	  case W_GTK_LISTITEM:
		  widget = gtk_list_item_new ();
		  break;
	  case W_GTK_MENU:
		  widget = gtk_menu_new ();
		  break;
	  case W_GTK_MENUBAR:
		  widget = gtk_menu_bar_new ();
		  break;
	  case W_GTK_MENUITEM:
		  widget = gtk_menu_item_new ();
		  break;
	  case W_GTK_NOTEBOOK:
		  widget = gtk_notebook_new ();
		  break;
	  case W_GTK_OPTIONMENU:
		  widget = gtk_option_menu_new ();
		  break;
	  case W_GTK_PROGRESSBAR:
		  widget = gtk_progress_bar_new ();
		  break;
	  case W_GTK_RADIOBUTTON:
		  widget = gtk_radio_button_new (NULL);
		  break;
	  case W_GTK_RANGE:
		  widget = gtk_vscrollbar_new (NULL);
		  break;
	  case W_GTK_RULER:
		  widget = gtk_vruler_new ();
		  break;
	  case W_GTK_SCALE:
		  widget = gtk_hscale_new (NULL);
		  break;
	  case W_GTK_SCROLL:
		  widget = gtk_hscrollbar_new (NULL);
		  break;
	  case W_GTK_SPINBUTTON:
		  widget = gtk_spin_button_new (NULL, 0, 1);
		  break;
	  case W_GTK_TEXT:
		  widget = gtk_text_new (NULL, NULL);
		  break;
	  case W_GTK_TOGGLEBUTTON:
		  widget = gtk_toggle_button_new ();
		  break;
	  case W_GTK_TREE:
		  widget = gtk_tree_new ();
		  break;
	  case W_GTK_TREEITEM:
		  widget = gtk_tree_item_new ();
		  break;
	  case W_GTK_VSCALE:
		  widget = gtk_vscale_new (NULL);
		  break;
	  case W_GTK_VSCROLL:
		  widget = gtk_vscrollbar_new (NULL);
		  break;
	  case W_GTK_WIDGET:
		  widget = gtk_widget_newv (gtk_widget_get_type (), 0,
					    NULL);
		  break;
	  case W_GTK_WINDOW:
		  widget = gtk_window_new (GTK_WINDOW_TOPLEVEL);
		  break;
	  default:
		  break;
	  }

	return widget;
}
