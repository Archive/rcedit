#ifndef __PREVIEW_H__
#define __PREVIEW_H__

#include "widgets.h"

typedef struct _PreviewDialog PreviewDialog;

typedef void (*PreviewDestroyFunc) (gpointer);
typedef void (*PreviewUpdateFunc) (PreviewDialog *);

struct _PreviewDialog
  {
	  GtkWidget *win;
	  GtkWidget *widget;
	  gpointer data;
	  PreviewDestroyFunc destroy_f;
	  PreviewUpdateFunc update_f;
  };

PreviewDialog *preview_dialog_new (gpointer data, PreviewUpdateFunc update_f,
				   PreviewDestroyFunc destroy_f);
void preview_dialog_set_type (PreviewDialog * data, WidgetType type);
void preview_dialog_destroy (PreviewDialog * dlg);

#endif /* __PREVIEW_H__ */
