
#include "simple.h"
#include "util.h"

static GtkTableClass *parent_class;

enum
  {
	  STATE_CHANGED,
	  LAST_SIGNAL
  };

guint color_state_signals[LAST_SIGNAL];

static void simple_color_state_class_init (SimpleColorStateClass * klass);
static void simple_color_state_init (GtkWidget * widget);
static void simple_color_state_destroy (GtkObject * object);

static void color_set_cb (GnomeColorPicker * cp, guint r, guint g, guint b,
			  guint a, SimpleColorForm * form);
static void default_handler_cb (SimpleColorState * state);

static void set_color (GtkWidget * sp, GdkColor * color);
static void style_color_set (GtkWidget * sp, GdkColor * color);

static void 
simple_color_state_class_init (SimpleColorStateClass * klass)
{
	GtkObjectClass *object_class;

	g_return_if_fail (klass != NULL);

	object_class = (GtkObjectClass *) klass;
	parent_class = gtk_type_class (gtk_table_get_type ());

	color_state_signals[STATE_CHANGED] = gtk_signal_new (
					     "_state_changed", GTK_RUN_LAST,
							 object_class->type,
		   GTK_SIGNAL_OFFSET (SimpleColorStateClass, state_changed),
			   gtk_signal_default_marshaller, GTK_TYPE_NONE, 0);
	gtk_object_class_add_signals (object_class, color_state_signals,
				      LAST_SIGNAL);
	object_class->destroy = simple_color_state_destroy;
	klass->state_changed = default_handler_cb;
}

GtkType 
simple_color_state_get_type (void)
{
	static guint simple_color_state_type = 0;

	if (!simple_color_state_type)
	  {
		  GtkTypeInfo simple_color_state_info =
		  {
			  "SimpleColorState",
			  sizeof (SimpleColorState),
			  sizeof (SimpleColorStateClass),
			  (GtkClassInitFunc) simple_color_state_class_init,
			  (GtkObjectInitFunc) simple_color_state_init,
			  (GtkArgSetFunc) NULL,
			  (GtkArgGetFunc) NULL
		  };

		  simple_color_state_type = gtk_type_unique (
						      gtk_table_get_type (),
						  &simple_color_state_info);
	  }
	return simple_color_state_type;
}


static void 
simple_color_state_init (GtkWidget * widget)
{
/* none here too (lazy) */
}

GtkWidget *
simple_color_state_new (GtkRcStyle * style, guint state)
{
	SimpleColorState *scstate;

	g_return_val_if_fail (style != NULL, NULL);
	g_return_val_if_fail (state >= GTK_STATE_NORMAL &&
			      state <= GTK_STATE_INSENSITIVE, NULL);

	scstate = SIMPLE_COLOR_STATE (gtk_type_new (simple_color_state_get_type ()));
	gtk_table_resize (GTK_TABLE (scstate), 4, 2);
	gtk_table_set_homogeneous (GTK_TABLE (scstate), FALSE);

	scstate->bg_label = gtk_label_new (_ ("Background"));
	gtk_table_attach_defaults (GTK_TABLE (scstate), scstate->bg_label,
				   0, 1, 0, 1);
	scstate->bg_table = gtk_table_new (2, 2, FALSE);
	scstate->bg_colorsel = gnome_color_picker_new ();
	gnome_color_picker_set_use_alpha (
			  GNOME_COLOR_PICKER (scstate->bg_colorsel), FALSE);

	gtk_table_attach_defaults (GTK_TABLE (scstate->bg_table),
				   scstate->bg_colorsel, 0, 1, 1, 2);
	gtk_table_attach_defaults (GTK_TABLE (scstate),
				   scstate->bg_table, 1, 2, 0, 1);
	gtk_widget_show_all (scstate->bg_table);

	scstate->fg_label = gtk_label_new (_ ("Foreground"));
	gtk_table_attach_defaults (GTK_TABLE (scstate),
				   scstate->fg_label, 0, 1, 1, 2);
	gtk_widget_show (scstate->fg_label);
	scstate->fg_colorsel = gnome_color_picker_new ();
	gnome_color_picker_set_use_alpha (
			  GNOME_COLOR_PICKER (scstate->fg_colorsel), FALSE);
	gtk_table_attach_defaults (GTK_TABLE (scstate),
				   scstate->fg_colorsel, 1, 2, 1, 2);
	gtk_widget_show (scstate->fg_colorsel);

	scstate->text_label = gtk_label_new (_ ("Text"));
	gtk_table_attach_defaults (GTK_TABLE (scstate),
				   scstate->text_label, 0, 1, 2, 3);
	gtk_widget_show (scstate->text_label);
	scstate->text_colorsel = gnome_color_picker_new ();
	gnome_color_picker_set_use_alpha (
			GNOME_COLOR_PICKER (scstate->text_colorsel), FALSE);
	gtk_table_attach_defaults (GTK_TABLE (scstate),
				   scstate->text_colorsel, 1, 2, 2, 3);
	gtk_widget_show (scstate->text_colorsel);

	scstate->base_label = gtk_label_new (_ ("Base"));
	gtk_table_attach_defaults (GTK_TABLE (scstate),
				   scstate->base_label, 0, 1, 3, 4);
	gtk_widget_show (scstate->base_label);
	scstate->base_colorsel = gnome_color_picker_new ();
	gnome_color_picker_set_use_alpha (
			GNOME_COLOR_PICKER (scstate->base_colorsel), FALSE);
	gtk_table_attach_defaults (GTK_TABLE (scstate),
				   scstate->base_colorsel, 1, 2, 3, 4);
	gtk_widget_show (scstate->base_colorsel);
	scstate->style = style;
	scstate->state = state;

	gtk_signal_connect (GTK_OBJECT (scstate->bg_colorsel), "color_set",
			    color_set_cb, scstate);
	gtk_signal_connect (GTK_OBJECT (scstate->fg_colorsel), "color_set",
			    color_set_cb, scstate);
	gtk_signal_connect (GTK_OBJECT (scstate->text_colorsel), "color_set",
			    color_set_cb, scstate);
	gtk_signal_connect (GTK_OBJECT (scstate->base_colorsel), "color_set",
			    color_set_cb, scstate);

	scstate->fs = NULL;

	simple_color_state_update (scstate);

	return GTK_WIDGET (scstate);
}

static void 
simple_color_state_destroy (GtkObject * object)
{
	SimpleColorState *scstate;

	g_return_if_fail (object != NULL);
	scstate = SIMPLE_COLOR_STATE (object);

	gtk_widget_destroy (scstate->bg_label);
	gtk_widget_destroy (scstate->bg_colorsel);
	gtk_widget_destroy (scstate->fg_label);
	gtk_widget_destroy (scstate->bg_table);
	gtk_widget_destroy (scstate->fg_colorsel);
	gtk_widget_destroy (scstate->text_label);
	gtk_widget_destroy (scstate->text_colorsel);
	gtk_widget_destroy (scstate->base_label);
	gtk_widget_destroy (scstate->base_colorsel);

	if (GTK_OBJECT_CLASS (parent_class)->destroy)
		(*GTK_OBJECT_CLASS (parent_class)->destroy) (object);
}

static void 
color_set_cb (GnomeColorPicker * cp, guint r, guint g, guint b,
	      guint a, SimpleColorForm * form)
{
	g_return_if_fail (form != NULL);

	g_print ("Emitting state_changed for color change\n");
	gtk_signal_emit (GTK_OBJECT (form), color_state_signals[STATE_CHANGED]);
}



static void 
set_color (GtkWidget * sp, GdkColor * color)
{
	g_return_if_fail (sp != NULL);
	g_return_if_fail (color != NULL);

	gnome_color_picker_set_i16 (GNOME_COLOR_PICKER (sp), color->red,
				    color->green, color->blue, 0);
}

void 
simple_color_state_update (SimpleColorState * scstate)
{
	guint state;
	GtkRcStyle *style;

	g_return_if_fail (scstate != NULL);

	state = scstate->state;
	style = scstate->style;

	set_color (scstate->bg_colorsel, &(style->bg[state]));
	set_color (scstate->fg_colorsel, &(style->fg[state]));
	set_color (scstate->text_colorsel, &(style->text[state]));
	set_color (scstate->base_colorsel, &(style->base[state]));
}

static void 
style_color_set (GtkWidget * sp, GdkColor * color)
{
	gushort r, g, b;
	gnome_color_picker_get_i16 (GNOME_COLOR_PICKER (sp), &r, &g, &b, NULL);
	color->red = r;
	color->green = g;
	color->blue = b;
}



static void 
default_handler_cb (SimpleColorState * state)
{
	g_return_if_fail (state != NULL);
	style_color_set (state->bg_colorsel,
			 &(state->style->bg[state->state]));
	style_color_set (state->fg_colorsel,
			 &(state->style->fg[state->state]));
	style_color_set (state->text_colorsel,
			 &(state->style->text[state->state]));
	style_color_set (state->base_colorsel,
			 &(state->style->base[state->state]));
}



