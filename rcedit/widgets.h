
#ifndef __WIDGETS_H__
#define __WIDGETS_H__

#include <gnome.h>

typedef enum
  {
	  W_GTK_ARROW,
	  W_GTK_BUTTON,
	  W_GTK_CHECKBUTTON,
	  W_GTK_CLIST,
	  W_GTK_CTREE,
	  W_GTK_CURVE,
	  W_GTK_DIALOG,
	  W_GTK_DRAWINGAREA,
	  W_GTK_EDITABLE,
	  W_GTK_ENTRY,
	  W_GTK_EVENTBOX,
	  W_GTK_FILESEL,
	  W_GTK_FONTSEL,
	  W_GTK_HSCALE,
	  W_GTK_HSCROLL,
	  W_GTK_ITEM,
	  W_GTK_LIST,
	  W_GTK_LISTITEM,
	  W_GTK_MENU,
	  W_GTK_MENUBAR,
	  W_GTK_MENUITEM,
	  W_GTK_NOTEBOOK,
	  W_GTK_OPTIONMENU,
	  W_GTK_PROGRESSBAR,
	  W_GTK_RADIOBUTTON,
	  W_GTK_RANGE,
	  W_GTK_RULER,
	  W_GTK_SCALE,
	  W_GTK_SCROLL,
	  W_GTK_SPINBUTTON,
	  W_GTK_TEXT,
	  W_GTK_TOGGLEBUTTON,
	  W_GTK_TREE,
	  W_GTK_TREEITEM,
	  W_GTK_VSCALE,
	  W_GTK_VSCROLL,
	  W_GTK_WIDGET,
	  W_GTK_WINDOW
  }
WidgetType;

GHashTable *widget_hash;
void widgets_init (void);
GtkWidget *create_widget (WidgetType type);

#endif /* __WIDGETS_H__ */
