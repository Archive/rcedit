#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include "type.h"
#include "mainapp.h"

static gboolean is_comment (gchar * str);
static gchar *strip_stars (gchar * str);	/* must free */
static gchar **get_path (gchar * str);
static WidgetType get_type (gchar * str);
static GtkRcStyle *copy_style (GtkStyle * style);

static gchar *
get_line (FILE * file)
{
	gchar *str;
	int i, length = 0;
	int c;

	g_return_val_if_fail (file != NULL, NULL);

	do
	  {
		  c = fgetc (file);
		  length++;
		  g_print ("%c", c);
	  }
	while (c != EOF && c != '\n');
	if (c == EOF)
		return NULL;
//      if (length == 0) return NULL;
	str = g_new (gchar, length);
	fseek (file, -length, SEEK_CUR);
	for (i = 0; i < length; i++)
	  {
		  str[i] = fgetc (file);
	  }
	str[i - 1] = 0;
	return str;
}

ThemeInfo *
theme_info_new (void)
{
	ThemeInfo *info;
	info = g_new (ThemeInfo, 1);
	info->file = NULL;
	info->engine = NULL;
	info->type = SIMPLE_COLOR;
	info->widgets = g_hash_table_new (g_direct_hash, g_direct_equal);
	g_hash_table_insert (info->widgets, GUINT_TO_POINTER (W_GTK_WIDGET),
			     NULL);
	return info;
}

void 
theme_info_destroy (ThemeInfo * info)
{
	g_return_if_fail (info != NULL);
	if (info->file)
		g_free (info->file);
	g_free (info);
}

ThemeInfo *
file_type (gchar * filename)
{
	FILE *file;
	gchar *str;
	ThemeInfo *info;
	gchar *tmp;

	g_return_val_if_fail (filename != NULL, NULL);

	file = fopen (filename, "r");
	g_return_val_if_fail (file != NULL, NULL);

	str = get_line (file);

	info = theme_info_new ();
	info->file = g_strdup (filename);
	while (str != NULL)
	  {			/* TODO do include thignies */
		  tmp = g_new (gchar, strlen (str) + 1);
		  if (strlen (str) && is_comment (str))
		    {
			    g_free (str);
			    g_free (tmp);
			    str = get_line (file);
			    continue;
		    }
		  if (strstr (str, "engine \""))
		    {
			    sscanf ("engine \"%s\"", tmp);
			    g_print ("Engine %s\n", str);
			    info->engine = g_strdup (tmp);
			    if (strcmp (tmp, "pixmap") == 0)
			      {
				      info->type = PIXMAP;
			      }
		    }
		  else if (strstr (str, "class \"") && !strstr (str, "GtkWidget")
			   && !strstr (str, "class \"*\""))
		    {
			    sscanf ("class \"%s\"", tmp);
			    g_print ("Complex! Class \n");
			    g_hash_table_insert (info->widgets,
			    GUINT_TO_POINTER (get_type (strip_stars (tmp))),
						 NULL);
			    info->type = COMPLEX_COLOR;
		    }
		  else if (strstr (str, "pixmap_path \""))
		    {
			    sscanf ("pixmap_path \"%s\"", tmp);
			    info->pixmap_path = get_path (tmp);
		    }
		  g_free (str);
		  g_free (tmp);
		  str = get_line (file);
	  }
	fclose (file);
	return info;
}

static GtkRcStyle *
copy_style (GtkStyle * style)
{
	GtkRcStyle *rc;
	int i;
	g_return_val_if_fail (style != NULL, NULL);
	rc = gtk_rc_style_new ();
	for (i = 0; i < 5; i++)
	  {
		  rc->fg[i] = style->fg[i];
		  rc->bg[i] = style->bg[i];
		  g_print ("Bg %i\n", rc->bg[i].red);
		  rc->text[i] = style->text[i];
		  rc->base[i] = style->base[i];
		  rc->bg_pixmap_name[i] = NULL;
	  }
	rc->font_name = "-adobe-helvetica-medium-r-normal--*-120-*-*-*-*-*-*";
	return rc;
}

GtkRcStyle *
get_default_style (WidgetType type)
{
	GtkWidget *widget;
	GtkRcStyle *rcstyle;

	widget = create_widget (type);
	gtk_widget_ensure_style (widget);

	g_return_val_if_fail (widget->style != NULL, NULL);
	rcstyle = widget->style->rc_style;

	if (rcstyle == NULL)
	  {
		  rcstyle = copy_style (gtk_rc_get_style (widget));
		  g_print ("Copied style\n");
	  }
	else
	  {
		  gtk_rc_style_ref (rcstyle);
	  }
//	g_print ("Ref %i\n", rcstyle->ref_count);
	gtk_widget_destroy (widget);
	return rcstyle;
}

static gboolean 
is_comment (gchar * str)
{
	int i;
	int len;

	g_return_val_if_fail (str != NULL, TRUE);
	len = strlen (str);

	for (i = 0; i < len; i++)
	  {
		  if (!isspace (str[i]))
		    {
			    if (str[i] == '#')
				    return TRUE;
			    else
				    return FALSE;
		    }
	  }
	return FALSE;
}

static gchar *
strip_stars (gchar * str)
{
	int len = strlen (str);
	gchar ret[len + 1];
	int i, x = 0;
	gboolean needs_prefix = FALSE;

	g_return_val_if_fail (str != NULL, NULL);

	for (i = 0; i < len; i++)
		if (str[i] != '*')
			ret[x++] = str[i];
	ret[x] = 0;
	len = strlen (ret);
	if (len > 3)
	  {
		  if (strncmp (ret, "Gtk", 3) != 0)
			  needs_prefix = TRUE;
		  else if (len > 5)
		    {
			    if (strncmp (ret, "Gnome", 5) == 0)
				    needs_prefix = TRUE;
		    }
	  }
	if (needs_prefix)
		return g_strconcat ("Gtk", ret, NULL);	/* Hrm */
	/* else */

	return g_strdup (ret);
}

static gchar **
get_path (gchar * str)
{
	GList *list = NULL;
	gchar *tmp = NULL;
	gchar **sstr;
	gchar **ret;
	int len;
	int i;

	g_return_val_if_fail (str != NULL, NULL);

	sstr = g_new (gchar *, 1);
	*sstr = g_strdup (str);
	do
	  {
		  tmp = strsep (sstr, G_SEARCHPATH_SEPARATOR_S);
		  if (tmp)
			  list = g_list_prepend (list, g_strdup (tmp));
	  }
	while (tmp);

	list = g_list_reverse (list);
	len = g_list_length (list);

	ret = g_new (gchar *, len);

	for (i = 0; i < (len - 1); i++)
	  {
		  ret[i] = list->data;
		  list = list->next;
		  g_list_free (list);
	  }
	g_list_free (list);
	g_free (sstr);

	return ret;
}

static WidgetType 
get_type (gchar * str)
{
	return GPOINTER_TO_UINT (g_hash_table_lookup (widget_hash, str));
}

static void 
foreach_func (gpointer key, gpointer value, ThemeInfo * user_data)
{
	g_return_if_fail (user_data != NULL);
	g_return_if_fail (value == NULL);

	g_hash_table_remove (user_data->widgets, key);
	g_hash_table_insert (user_data->widgets, key, get_default_style (GPOINTER_TO_UINT (key)));
}

void 
theme_info_realize (ThemeInfo * info)
{
	g_return_if_fail (info != NULL);
	g_hash_table_freeze (info->widgets);
	g_hash_table_foreach (info->widgets, (GHFunc) foreach_func, info);
	g_hash_table_thaw (info->widgets);
}
