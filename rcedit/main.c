
#include "mainapp.h"

int 
main (int argc, char **argv)
{
	MainApp *app;

	gnome_init ("rcedit", VERSION, argc, argv);
	widgets_init ();
	app = main_app_new (theme_info_new ());
	gtk_widget_show_all (app->app);
	gtk_main ();

	return 0;
}
