
#ifndef __SIMPLE_H__
#define __SIMPLE_H__

#include <gnome.h>

#define SIMPLE_COLOR_STATE(obj)		GTK_CHECK_CAST(obj, simple_color_state_get_type(), SimpleColorState)
#define SIMPLE_COLOR_STATE_CLASS(klass)	GTK_CHECK_CLASS_CAST (klass, simple_color_state_get_type(), SimpleColorStateClass)
#define SIMPLE_IS_COLOR_STATE(obj)	GTK_CHECK_TYPE (obj, simple_color_state_get_type())

typedef struct _SimpleColorState SimpleColorState;
typedef struct _SimpleColorStateClass SimpleColorStateClass;

struct _SimpleColorState
  {
	  GtkTable table;
	  GtkWidget *bg_label;
	  GtkWidget *bg_table;
	  GtkWidget *bg_colorsel;
	  GtkWidget *fg_label;
	  GtkWidget *fg_colorsel;
	  GtkWidget *text_label;
	  GtkWidget *text_colorsel;
	  GtkWidget *base_label;
	  GtkWidget *base_colorsel;
	  GtkWidget *fs;
	  GtkRcStyle *style;
	  guint state;
  };

struct _SimpleColorStateClass
  {
	  GtkTableClass parent_class;
	  void (*state_changed) (SimpleColorState * state);
  };

GtkType simple_color_state_get_type (void);
GtkWidget *simple_color_state_new (GtkRcStyle * style, guint state);
void simple_color_state_update (SimpleColorState * scstate);

#define SIMPLE_COLOR_FORM(obj)		GTK_CHECK_CAST(obj, simple_color_form_get_type(), SimpleColorForm)
#define SIMPLE_COLOR_FORM_CLASS(klass)	GTK_CHECK_CLASS_CAST (klass, simple_color_form_get_type(), SimpleColorFormClass)
#define SIMPLE_IS_COLOR_FORM(obj)	GTK_CHECK_TYPE (obj, simple_color_form_get_type())

typedef struct _SimpleColorForm SimpleColorForm;
typedef struct _SimpleColorFormClass SimpleColorFormClass;

struct _SimpleColorForm
  {
	  GtkVBox vbox;
	  GtkWidget *font_hbox;
	  GtkWidget *font_label;
	  GtkWidget *font_picker;
	  GtkWidget *notebook;
	  GtkWidget *notebook_labels[5];
	  GtkWidget *states[5];
	  GtkRcStyle *style;
  };

struct _SimpleColorFormClass
  {
	  GtkVBoxClass parent_class;
	  void (*form_changed) (SimpleColorForm * form);
  };

GtkType simple_color_form_get_type (void);
GtkWidget *simple_color_form_new (GtkRcStyle * style);
void simple_color_form_update (SimpleColorForm * form);
void simple_color_form_set_style (SimpleColorForm * form, GtkRcStyle * style,
				  gboolean update);

#endif /* __SIMPLE_H__ */
