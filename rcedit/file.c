
#include <gnome.h>

static void ok_cb (GtkWidget * widget, gpointer data);
static void cancel_cb (GtkWidget * widget, gpointer data);
gchar * get_fname(gchar *title, gchar *fdefault);
gchar *
get_fname (gchar * title, gchar * fdefault)
{
	GtkWidget *fs = gtk_file_selection_new (title);
	char *tmp;
	gtk_file_selection_set_filename (GTK_FILE_SELECTION (fs), fdefault);
	gtk_signal_connect (GTK_OBJECT (GTK_FILE_SELECTION (fs)->ok_button),
			    "clicked", ok_cb, fs);
	gtk_signal_connect (GTK_OBJECT (GTK_FILE_SELECTION (fs)->cancel_button),
			    "clicked", cancel_cb, fs);
	gtk_signal_connect (GTK_OBJECT (fs), "delete_event", cancel_cb,
			    fs);
	gtk_object_set_user_data (GTK_OBJECT (fs), NULL);
	gtk_widget_show (fs);
	gtk_main ();
	if (!gtk_object_get_user_data (GTK_OBJECT (fs)))
		tmp = NULL;
	else
		tmp = g_strdup (gtk_file_selection_get_filename (GTK_FILE_SELECTION (fs)));
	gtk_widget_destroy (fs);
	return tmp;
}

static void 
ok_cb (GtkWidget * widget, gpointer data)
{
	gtk_object_set_user_data (GTK_OBJECT (data), GINT_TO_POINTER (1));
	gtk_main_quit ();
}

static void 
cancel_cb (GtkWidget * widget, gpointer data)
{
	gtk_main_quit ();
}
