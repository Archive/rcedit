
#ifndef __COMPLEX_H__
#define __COMPLEX_H__

#include <gnome.h>
#include "widdialog.h"
#include "type.h"

#define COMPLEX_COLOR_FORM(obj)		GTK_CHECK_CAST(obj, complex_color_form_get_type(), ComplexColorForm)
#define COMPLEX_COLOR_FORM_CLASS(klass)	GTK_CHECK_CLASS_CAST(klass, complex_color_form_get_type(), ComplexColorFormClass)
#define COMPLEX_IS_COLOR_FORM(obj)	GTK_CHECK_TYPE(obj, complex_color_form_get_type())

typedef struct _ComplexColorForm ComplexColorForm;
typedef struct _ComplexColorFormClass ComplexColorFormClass;

struct _ComplexColorForm
  {
	  GtkContainer container;
	  ThemeInfo *info;
	  GtkWidget *simple;
//	  WidgetDialog *dlg;
	  GList *new_styles;
  };

struct _ComplexColorFormClass
  {
	  GtkContainerClass parent_class;
  };

GtkType complex_color_form_get_type ();
GtkWidget *complex_color_form_new (ThemeInfo * info);
void complex_color_form_update (ComplexColorForm * form);
void complex_color_form_set_info (ComplexColorForm * form, ThemeInfo * info);

#endif /* __COMPLEX_H__ */
