#include <sys/stat.h>
#include <unistd.h>
#include "util.h"

gboolean 
file_exists (gchar * path, gchar * file)
{
	struct stat st;
	gchar *str;
	gboolean ret;

	g_return_val_if_fail (file != NULL, FALSE);
	if (path == NULL)
		path = "";
	str = g_strconcat (path, G_DIR_SEPARATOR_S, file, NULL);

	if (stat (file, &st) == 0)
		ret = TRUE;
	else
		ret = FALSE;

	g_free (str);
	return ret;
}


