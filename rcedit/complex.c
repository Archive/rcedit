#include "complex.h"

static GtkContainerClass parent_class;

static void complex_color_form_class_init (ComplexColorFormClass * klass);
static void complex_color_form_init (GtkWidget * widget);
static void complex_color_form_destroy (GtkObject * object);


static void 
complex_color_form_class_init (ComplexColorFormClass * klass)
{
	GtkObjectClass *object_class;

	g_return_if_fail (klass != NULL);

	object_class = (GtkObjectClass *) klass;
	parent_class = gtk_type_class (gtk_container_get_type ());

	object_class->destroy = complex_color_form_destroy;
}

GtkType 
complex_color_form_get_type (void)
{
	static guint complex_color_form_type = 0;

	if (!complex_color_form_type)
	  {
		  GtkTypeInfo complex_color_form_info =
		  {
			  "ComplexColorForm",
			  sizeof (ComplexColorForm),
			  sizeof (ComplexColorFormClass),
			  (GtkClassInitFunc) complex_color_form_class_init,
			  (GtkObjectInitFunc) complex_color_form_init,
			  (GtkArgSetFunc) NULL,
			  (GtkArgGetFunc) NULL
		  };

		  complex_color_form_type = gtk_type_unique (
						  gtk_container_get_type (),
						  &complex_color_form_info);
	  }
	return complex_color_form_type;
}

static void 
complex_color_form_init (GtkWidget * widget)
{
/* Getting used to this by now? :-) */
}

GtkWidget *
complex_color_form_new (ThemeInfo * info)
{
	ComplexColorForm *form;

	g_return_val_if_fail (info != NULL, NULL);

	form = COMPLEX_COLOR_FORM (gtk_type_new (complex_color_form_get_type ()));
	form->dlg = widget_dialog_new ();

	complex_color_form_set_info (form, info);
	return GTK_WIDGET (form);
}

void 
complex_color_form_set_info (ComplexColorForm * form, ThemeInfo * info)
{
	g_return_if_fail (form != NULL);
	g_return_if_fail (info != NULL);
	g_return_if_fail (form->info != info);

	form->info = info;
	theme_info_realize (info);
	complex_color_form_update (form);
}
