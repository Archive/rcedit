#include "preview.h"
#include "widgets.h"

PreviewDialog *
preview_dialog_new (gpointer data, PreviewUpdateFunc update_f,
		    PreviewDestroyFunc destroy_f)
{
	PreviewDialog *dlg = g_new (PreviewDialog, 1);
	dlg->win = gtk_window_new (GTK_WINDOW_TOPLEVEL);
	dlg->widget = NULL;
	dlg->data = data;
	dlg->update_f = update_f;
	dlg->destroy_f = destroy_f;
	gtk_window_set_title (GTK_WINDOW (dlg->win), _ ("Preview"));
	if (update_f)
		update_f (dlg);
	gtk_widget_show (dlg->win);
	return dlg;
}

void 
preview_dialog_set_type (PreviewDialog * data, WidgetType type)
{
	g_return_if_fail (data != NULL);

	if (data->widget)
	  {
		  gtk_container_remove (GTK_CONTAINER (data->win),
					data->widget);
		  gtk_widget_destroy (data->widget);
	  }
	data->widget = create_widget (type);
	if (data->update_f)
		data->update_f (data);
	gtk_widget_show (data->widget);
	gtk_container_add (GTK_CONTAINER (data->win),
			   data->widget);
}

void 
preview_dialog_destroy (PreviewDialog * dlg)
{
	g_return_if_fail (dlg != NULL);

	if (dlg->destroy_f)
		dlg->destroy_f (dlg->data);
	if (dlg->widget)
		gtk_widget_destroy (dlg->widget);
	gtk_widget_destroy (dlg->win);
	g_free (dlg);
}
