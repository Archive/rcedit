
#ifndef __MAIN_APP_H__
#define __MAIN_APP_H__

#include <gnome.h>
#include <config.h>
#include "simple.h"
#include "complex.h"
#include "widgets.h"
#include "type.h"
#include "preview.h"

typedef struct
  {
	  GtkWidget *app;
	  ThemeInfo *info;
	  GtkWidget *form;
	  GtkWidget *container;
	  PreviewDialog *simple_dlg;
  }
MainApp;

MainApp *main_app_new (ThemeInfo * info);
MainApp *main_app_new_from_file (gchar * file);
void main_app_save_to_file (MainApp * mapp);
void main_app_reload_file (MainApp * mapp);
void main_app_reload_type (MainApp * mapp);
void main_app_destroy (MainApp * mapp);

#endif /* __MAIN_APP_H__ */
