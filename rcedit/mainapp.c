#include "mainapp.h"
#include "rcstyle-io.h"
#include <stdio.h>

static void file_save_cb (GtkWidget * widget, MainApp * mapp);
static void file_save_as_cb (GtkWidget * widget, MainApp * mapp);
static void file_open_cb (GtkWidget * widget, MainApp * mapp);
static void file_exit_cb (GtkWidget * widget, MainApp * mapp);

/* Forward decl */
gchar *get_fname (gchar * title, gchar * fdefault);

MainApp *
main_app_new (ThemeInfo * info)
{
	MainApp *mapp = g_new0 (MainApp, 1);
	GnomeUIInfo mapp_file_menu[] =
	{
	/* Should be a tree? */
		GNOMEUIINFO_MENU_NEW_ITEM (_ ("New"), _ ("Create a new document"), NULL, mapp),
		GNOMEUIINFO_MENU_OPEN_ITEM (file_open_cb, mapp),
		GNOMEUIINFO_MENU_SAVE_ITEM (file_save_cb, mapp),
		GNOMEUIINFO_MENU_SAVE_AS_ITEM (file_save_as_cb, mapp),
		GNOMEUIINFO_SEPARATOR,
		GNOMEUIINFO_MENU_EXIT_ITEM (file_exit_cb, mapp),
		GNOMEUIINFO_END
	};
	GnomeUIInfo mapp_menu[] =
	{
		GNOMEUIINFO_MENU_FILE_TREE (mapp_file_menu),
		GNOMEUIINFO_END
	};

	if (info == NULL)
		g_free (mapp);	/* and.. */
	g_return_val_if_fail (info != NULL, NULL);

	mapp->app = gnome_app_new ("rcedit", "Resource Editor");
	mapp->info = info;
	mapp->form = NULL;
	mapp->simple_dlg = NULL;
	mapp->container = gtk_table_new (1, 1, FALSE);
	gnome_app_set_contents (GNOME_APP (mapp->app),
				mapp->container);
	gtk_widget_show (mapp->container);
	gnome_app_create_menus (GNOME_APP (mapp->app), mapp_menu);
	main_app_reload_type (mapp);
	return mapp;
}

void 
main_app_reload_file (MainApp * mapp)
{
	ThemeInfo *old;
	g_return_if_fail (mapp != NULL);
	g_return_if_fail (mapp->info != NULL);
	old = mapp->info;
	mapp->info = file_type (mapp->info->file);
	g_print ("Called file_type\n");
	theme_info_destroy (old);
	main_app_reload_type (mapp);
}

void 
main_app_reload_type (MainApp * mapp)
{
	g_return_if_fail (mapp != NULL);
	g_return_if_fail (mapp->info != NULL);

	if (mapp->form)
	  {
		  gtk_widget_destroy (mapp->form);
	  }
	gtk_rc_reparse_all ();
	if (mapp->info->file)
		gtk_rc_parse (mapp->info->file);

	switch (mapp->info->type)
	  {
	  case SIMPLE_COLOR:
	  case COMPLEX_COLOR:	/* Until support implemented */
	  case PIXMAP:
		  mapp->form = simple_color_form_new (
					   get_default_style (W_GTK_WIDGET));
		  gtk_table_attach_defaults (GTK_TABLE (mapp->container),
					     mapp->form, 0, 1, 0, 1);
		  break;
	  }
	gtk_widget_show (mapp->form);
}

MainApp *
main_app_new_from_file (gchar * file)
{
	ThemeInfo *info;
	MainApp *mapp;

	g_return_val_if_fail (file != NULL, NULL);

	info = file_type (file);
	mapp = main_app_new (info);
	return mapp;
}

void 
main_app_destroy (MainApp * mapp)
{
	g_return_if_fail (mapp != NULL);

	gtk_widget_destroy (mapp->form);
	gtk_widget_destroy (mapp->container);
	gtk_widget_destroy (mapp->app);
	g_free (mapp->info);

	g_free (mapp);
}

static void 
file_save_cb (GtkWidget * widget, MainApp * mapp)
{
	if (mapp->info->file != NULL)
		main_app_save_to_file (mapp);
	else
		file_save_as_cb (widget, mapp);
}

static void 
file_save_as_cb (GtkWidget * widget, MainApp * mapp)
{
	char *tmp = get_fname ("Save As", "");
	if (!tmp)
		return;
	if (mapp->info->file)
		g_free (mapp->info->file);
	mapp->info->file = tmp;
	main_app_save_to_file (mapp);
}

void 
main_app_save_to_file (MainApp * mapp)
{
	FILE *file;
	g_return_if_fail (mapp != NULL);
	g_return_if_fail (mapp->info->file != NULL);
	file = fopen (mapp->info->file, "w");
	g_return_if_fail (file != NULL);

	switch (mapp->info->type)
	  {
	  case SIMPLE_COLOR:
		  SIMPLE_COLOR_FORM (mapp->form)->style->name = "default";
		  print_style (SIMPLE_COLOR_FORM (mapp->form)->style,
			       "GtkWidget", mapp->info->engine,
			       file, 0);
		  break;
	  case COMPLEX_COLOR:
		  break;
	  case PIXMAP:
		  break;
	  }
	fclose (file);
}

static void 
file_open_cb (GtkWidget * widget, MainApp * mapp)
{
	char *tmp = get_fname ("Open", "");
	if (!tmp)
		return;
	if (mapp->info->file)
		g_free (mapp->info->file);
	mapp->info->file = tmp;
	main_app_reload_file (mapp);
}

static void 
file_exit_cb (GtkWidget * widget, MainApp * mapp)
{
	main_app_destroy (mapp);
	gtk_main_quit ();
}
