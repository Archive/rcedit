
#include "rcstyle-io.h"

static void print_gdk_color (GdkColor * color, FILE * file);
static void print_resource (GdkColor * colors, gchar * name, FILE * file,
			    guint indent);

void 
print_style (GtkRcStyle * style, gchar * widget, gchar * engine, 
	     FILE * file, guint indent)
{

	gchar *spacing;
	gchar *dblspc;

	g_return_if_fail (style != NULL);
	g_return_if_fail (style->name != NULL);
	g_return_if_fail (file != NULL);

	spacing = get_indent (indent);
	dblspc = get_indent (indent + 1);

	fprintf (file, "%sstyle \"%s\"\n", spacing, style->name);
	fprintf (file, "%s{\n", spacing);

	if (style->font_name)
		fprintf (file, "%sfont = \"%s\"\n", dblspc, style->font_name);
	if (style->fontset_name)
		fprintf (file, "%sfontset = \"%s\"\n", dblspc,
			 style->fontset_name);
	if (engine)
		fprintf (file, "%sengine \"%s\" {}\n", dblspc, engine);

	indent++;
	print_resource (style->fg, "fg", file, indent);
	print_resource (style->bg, "bg", file, indent);
	print_resource (style->text, "text", file, indent);
	print_resource (style->base, "base", file, indent);



	fprintf (file, "%s}\n", spacing);
	fprintf (file, "%sclass \"%s\" style \"%s\"\n", spacing, widget,
		 style->name);

	g_free (spacing);
	g_free (dblspc);
}

static void 
print_resource (GdkColor * colors, gchar * name, FILE * file,
		guint indent)
{

	gchar *spacing;
	static gchar *states[] =
	{"NORMAL", "ACTIVE", "PRELIGHT", "SELECTED", "INSENSITIVE"};
	int i;

	g_return_if_fail (colors != NULL);
	g_return_if_fail (name != NULL);
	g_return_if_fail (file != NULL);

	spacing = get_indent (indent);

	for (i = 0; i < 5; i++)
	  {
/*              if (colors[i]) { */
		  fprintf (file, "%s%s[%s] = \"", spacing, name,
			   states[i]);
		  print_gdk_color (&(colors[i]), file);
		  fprintf (file, "\"\n");
/*              } */
	  }
	g_free (spacing);
}

static void 
print_gdk_color (GdkColor * color, FILE * file)
{
	int r, g, b;

	g_return_if_fail (color != NULL);
	g_return_if_fail (file != NULL);

	r = (color->red * 255) / 65535;
	g = (color->green * 255) / 65535;
	b = (color->blue * 255) / 65535;

	fprintf (file, "#%02x%02x%02x", r, g, b);
}

gchar *
get_indent (guint indent)
{
	gchar *ret = g_new(gchar, indent + 1);
	int i;

	for (i = 0; i < indent; i++)
		ret[i] = ' ';
	ret[i] = 0;

	return ret;
}
