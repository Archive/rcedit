#include "rcstyle-io.h"

#define BOOLEAN(foo) ((foo) ? "TRUE" : "FALSE")

static void print_border (gchar * name, GdkImlibBorder * border, FILE * file,
			  guint indent);

void 
print_pixmap (ThemeData * data, FILE * file, guint indent)
{
	gchar *spacing;
	gchar *spacing2;
	gchar *spacing3;
	GList *list;
	theme_image *image;
	static gchar *funcs[] =
	{"HLINE", "VLINE", "SHADOW", "POLYGON",
	 "ARROW", "DIAMOND", "OVAL", "STRING", "BOX", "FLAT_BOX", "CHECK",
	 "OPTION", "CROSS", "RAMP", "TAB", "SHADOW_GAP", "BOX_GAP", "EXTENSION",
	 "FOCUS", "SLIDER", "ENTRY", "HANDLE"};
	static gchar *pos[] =
	{"LEFT", "RIGHT", "TOP", "BOTTOM"};
	static gchar *orient[] =
	{"HORIZONTAL", "VERTICAL"};
	static gchar *state[] =
	{"NORMAL", "ACTIVE", "PRELIGHT", "SELECTED",
	 "INSENSITIVE"};
	static gchar *shadow[] =
	{"NONE", "IN", "OUT", "ETCHED_IN",
	 "ETCHED_OUT"};
	static gchar *arrow[] =
	{"UP", "DOWN", "LEFT", "RIGHT"};

	g_return_if_fail (data != NULL);
	g_return_if_fail (file != NULL);

	spacing = get_indent (indent);
	spacing2 = get_indent (indent + 1);
	spacing3 = get_indent (indent + 2);
	list = data->img_list;

	fprintf (file, "%sengine \"pixmap\" {\n", spacing);

	while (list != NULL)
	  {
		  image = list->data;
		  if (image == NULL)
			  continue;
		  fprintf (file, "%simage {\n", spacing2);
		  if (image->function != -1)
			  fprintf (file, "%sfunction = %s\n", spacing3,
				   funcs[image->function - TOKEN_D_HLINE]);
		  fprintf (file, "%srecolorable = %s\n", spacing3,
			   BOOLEAN (image->recolorable));
		  if (image->detail)
			  fprintf (file, "%sdetail = \"%s\"\n", spacing3,
				   image->detail);
		  if (image->file)
		    {
			    fprintf (file, "%sfile = \"%s\"\n", spacing3,
				     image->file);
			    print_border ("border", &(image->border), file,
					  indent + 2);
			    fprintf (file, "%sstretch = %s\n", spacing3,
				     BOOLEAN (image->stretch));
		    }
		  if (image->overlay_file)
		    {
			    fprintf (file, "%sfile = \"%s\"\n", spacing3,
				     image->overlay_file);
			    print_border ("overlay_border", &(image->overlay_border),
					  file, indent + 2);
			    fprintf (file, "%soverlay_stretch = %s\n", spacing3,
				     BOOLEAN (image->overlay_stretch));
		    }
		  if (image->gap_file)
		    {
			    fprintf (file, "%sgap_file = \"%s\"\n", spacing3,
				     image->gap_file);
			    print_border ("gap_border", &(image->gap_border),
					  file, indent + 2);
		    }
		  if (image->gap_start_file)
		    {
			    fprintf (file, "%sgap_start_file = \"%s\"\n", spacing3,
				     image->gap_start_file);
			    print_border ("gap_start_border",
			      &(image->gap_start_border), file, indent + 2);
		    }
		  if (image->__gap_side)
			  fprintf (file, "%sgap_side = %s\n", spacing3,
				   pos[image->gap_side]);
		  if (image->__orientation)
			  fprintf (file, "%sorientation = %s\n", spacing3,
				   orient[image->orientation]);
		  if (image->__state)
			  fprintf (file, "%sstate = %s\n", spacing3,
				   state[image->state]);
		  if (image->__shadow)
			  fprintf (file, "%sshadow = %s\n", spacing3,
				   shadow[image->shadow]);
		  if (image->__arrow_direction)
			  fprintf (file, "%sarrow_direction = %s\n", spacing3,
				   arrow[image->arrow_direction]);

		  fprintf (file, "%s}\n", spacing2);
		  list = list->next;
	  }
	fprintf (file, "%s}\n", spacing);

	g_free (spacing);
	g_free (spacing2);
	g_free (spacing3);
}

static void 
print_border (gchar * name, GdkImlibBorder * border, FILE * file,
	      guint indent)
{
	gchar *spacing;

	g_return_if_fail (name != NULL);
	g_return_if_fail (border != NULL);
	g_return_if_fail (file != NULL);

	spacing = get_indent (indent);
	fprintf (file, "%s%s = { %i, %i, %i, %i }\n", spacing, name,
		 border->left, border->right, border->top, border->bottom);
	g_free (spacing);
}
