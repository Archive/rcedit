#include "simple.h"

static GtkVBoxClass *parent_class;

enum
  {
	  FORM_CHANGED,
	  LAST_SIGNAL
  };

guint color_form_signals[LAST_SIGNAL];

static void simple_color_form_class_init (SimpleColorFormClass * klass);
static void simple_color_form_init (GtkWidget * widget);
static void simple_color_form_destroy (GtkObject * object);

static void default_handler_cb (SimpleColorForm * form);
static void font_set_cb (GtkWidget * widget, gchar * font_name,
			 SimpleColorForm * form);
static void state_changed_cb (GtkWidget * widget, SimpleColorForm * form);

static void 
simple_color_form_class_init (SimpleColorFormClass * klass)
{
	GtkObjectClass *object_class;

	g_return_if_fail (klass != NULL);

	object_class = (GtkObjectClass *) klass;
	parent_class = gtk_type_class (gtk_vbox_get_type ());

	color_form_signals[FORM_CHANGED] = gtk_signal_new (
					       "form_changed", GTK_RUN_LAST,
							 object_class->type,
		     GTK_SIGNAL_OFFSET (SimpleColorFormClass, form_changed),
			   gtk_signal_default_marshaller, GTK_TYPE_NONE, 0);
	gtk_object_class_add_signals (object_class, color_form_signals,
				      LAST_SIGNAL);
	object_class->destroy = simple_color_form_destroy;
	klass->form_changed = default_handler_cb;
}

GtkType 
simple_color_form_get_type (void)
{
	static guint simple_color_form_type = 0;

	if (!simple_color_form_type)
	  {
		  GtkTypeInfo simple_color_form_info =
		  {
			  "SimpleColorForm",
			  sizeof (SimpleColorForm),
			  sizeof (SimpleColorFormClass),
			  (GtkClassInitFunc) simple_color_form_class_init,
			  (GtkObjectInitFunc) simple_color_form_init,
			  (GtkArgSetFunc) NULL,
			  (GtkArgGetFunc) NULL
		  };

		  simple_color_form_type = gtk_type_unique (
						       gtk_vbox_get_type (),
						   &simple_color_form_info);
	  }
	return simple_color_form_type;
}


static void 
simple_color_form_init (GtkWidget * widget)
{
/* none here too (lazy) */
}

GtkWidget *
simple_color_form_new (GtkRcStyle * style)
{
	SimpleColorForm *form;
	int i;
	static gchar *labels[5] =
	{_ ("Normal"), _ ("Active"), _ ("Prelight"),
	 _ ("Selected"), _ ("Insensitive")};

	g_return_val_if_fail (style != NULL, NULL);

	form = SIMPLE_COLOR_FORM (gtk_type_new (simple_color_form_get_type ()));
	gtk_box_set_homogeneous (GTK_BOX (form), FALSE);
	gtk_box_set_spacing (GTK_BOX (form), 0);

//      gtk_rc_style_ref(style);

	form->font_hbox = gtk_hbox_new (FALSE, 0);
	form->font_label = gtk_label_new (_ ("Font"));
	gtk_box_pack_start (GTK_BOX (form->font_hbox), form->font_label,
			    FALSE, FALSE, 5);
	form->font_picker = gnome_font_picker_new ();
	gnome_font_picker_set_mode (GNOME_FONT_PICKER (form->font_picker),
				    GNOME_FONT_PICKER_MODE_FONT_INFO);
	gnome_font_picker_fi_set_use_font_in_label (
			   GNOME_FONT_PICKER (form->font_picker), TRUE, 12);
	gtk_signal_connect (GTK_OBJECT (form->font_picker), "font_set",
			    font_set_cb, form);
	gtk_box_pack_start (GTK_BOX (form->font_hbox), form->font_picker,
			    TRUE, TRUE, 5);
	gtk_box_pack_start (GTK_BOX (form), form->font_hbox,
			    FALSE, FALSE, 5);
	gtk_widget_show_all (form->font_hbox);

	form->notebook = gtk_notebook_new ();
	for (i = 0; i < 5; i++)
	  {
		  form->states[i] = simple_color_state_new (style, i);
		  gtk_signal_connect (GTK_OBJECT (form->states[i]),
				  "_state_changed", state_changed_cb, form);
		  form->notebook_labels[i] = gtk_label_new (labels[i]);
		  gtk_notebook_append_page (GTK_NOTEBOOK (form->notebook),
				 form->states[i], form->notebook_labels[i]);
	  }
	gtk_widget_show_all (form->notebook);
	gtk_box_pack_start (GTK_BOX (form), form->notebook, TRUE, TRUE, 5);

	form->style = style;


	simple_color_form_update (form);

	return GTK_WIDGET (form);
}

static void 
simple_color_form_destroy (GtkObject * object)
{
	SimpleColorForm *form;
	int i;

	g_return_if_fail (object != NULL);
	form = SIMPLE_COLOR_FORM (object);

	gtk_widget_destroy (form->font_label);
	gtk_widget_destroy (form->font_picker);
	gtk_widget_destroy (form->font_hbox);

	for (i = 0; i < 5; i++)
	  {
		  gtk_widget_destroy (form->notebook_labels[i]);
		  gtk_widget_destroy (form->states[i]);
	  }
	gtk_widget_destroy (form->notebook);

	if (GTK_OBJECT_CLASS (parent_class)->destroy)
		(*GTK_OBJECT_CLASS (parent_class)->destroy) (object);
}

void 
simple_color_form_update (SimpleColorForm * form)
{
	int i;
	g_return_if_fail (form != NULL);
	for (i = 0; i < 5; i++)
	  {
		  simple_color_state_update (SIMPLE_COLOR_STATE (form->states[i]));
	  }
	gnome_font_picker_set_font_name (GNOME_FONT_PICKER (form->font_picker),
					 form->style->font_name);
	g_print ("Font %s\n", gnome_font_picker_get_font_name (GNOME_FONT_PICKER (
						       form->font_picker)));
}

void 
simple_color_form_set_style (SimpleColorForm * form, GtkRcStyle * style,
			     gboolean update)
{
	int i;
	g_return_if_fail (form != NULL);
	g_return_if_fail (style != NULL);

	form->style = style;

	for (i = 0; i < 5; i++)
	  {
		  SIMPLE_COLOR_STATE (form->states[i])->style = style;
	  }
	if (update)
		simple_color_form_update (form);
}

static void 
default_handler_cb (SimpleColorForm * form)
{
	g_return_if_fail (form != NULL);
	form->style->font_name = gnome_font_picker_get_font_name (
				     GNOME_FONT_PICKER (form->font_picker));
}

static void 
font_set_cb (GtkWidget * widget, gchar * font_name,
	     SimpleColorForm * form)
{
	g_print ("Emitting form_changed for font_set event\n");
	gtk_signal_emit (GTK_OBJECT (form), color_form_signals[FORM_CHANGED]);
}

static void 
state_changed_cb (GtkWidget * widget, SimpleColorForm * form)
{
	g_print ("Emitting form_changed for state_changed event\n");
	gtk_signal_emit (GTK_OBJECT (form), color_form_signals[FORM_CHANGED]);
}
