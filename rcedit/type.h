
#ifndef __TYPE_H__
#define __TYPE_H__

#include <gnome.h>
#include "widgets.h"

typedef struct
  {
	  enum
	    {
		    SIMPLE_COLOR,
		    COMPLEX_COLOR,
		    PIXMAP
	    }
	  type;
	  gchar *file;
	  gchar *engine;
	  gchar **pixmap_path;
	  GHashTable *widgets;
  }
ThemeInfo;

ThemeInfo *theme_info_new (void);
void theme_info_realize (ThemeInfo * info);	/* Uses Gtk calls to fill in styles */
ThemeInfo *file_type (gchar * filename);
GtkRcStyle *get_default_style (WidgetType type);
void theme_info_destroy (ThemeInfo * info);

#endif /* __TYPE_H__ */
