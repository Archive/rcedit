#include "widdialog.h"

static void free_tree (GNode * node);

static typedef struct
  {
	  char *name;
	  WidgetType type;
  }
WidgetInfo;

static WidgetInfo *
widget_info_new (char *name, WidgetType type)
{
	WidgetInfo *info = g_new (WidgetInfo, 1);
	info->name = name;
	info->type = type;
	return info;
}

static GNode *
tree_new ()
{
	GNode *root = g_node_new (widget_info_new (_ ("Widgets"), W_GTK_WIDGET));
	GNode *button, *toggle, *check, *clist, *draw, *edit, *entry;
	GNode *item, *notebook, *range, *scale, *scroll, *window, *dialog;

	g_node_append (root, g_node_new (
			      widget_info_new (_ ("Arrows"), W_GTK_ARROW)));
	g_node_append (root, button = g_node_new (widget_info_new (_ ("Buttons"),
							    W_GTK_BUTTON)));
	g_node_append (button, toggle = g_node_new (
	       widget_info_new (_ ("Toggle Buttons"), W_GTK_TOGGLEBUTTON)));
	g_node_append (toggle, check = g_node_new (
		 widget_info_new (_ ("Check Buttons"), W_GTK_CHECKBUTTON)));
	g_node_append (check, g_node_new (widget_info_new (_ ("Radio Buttons"),
						       W_GTK_RADIOBUTTON)));
	g_node_append (button, g_node_new (
		   widget_info_new (_ ("Option Menus"), W_GTK_OPTIONMENU)));
	g_node_append (root, clist = g_node_new (
		      widget_info_new (_ ("Columned Lists"), W_GTK_CLIST)));
	g_node_append (clist, g_node_new (
		      widget_info_new (_ ("Columned Trees"), W_GTK_CTREE)));
	g_node_append (root, draw = g_node_new (
		 widget_info_new (_ ("Drawing Areas"), W_GTK_DRAWINGAREA)));
	g_node_append (draw, g_node_new (widget_info_new (_ ("Curves"),
							  W_GTK_CURVE)));
	g_node_append (root, edit = g_node_new (
			widget_info_new (_ ("Editables"), W_GTK_EDITABLE)));
	g_node_append (edit, entry = g_node_new (
			     widget_info_new (_ ("Entries"), W_GTK_ENTRY)));
	g_node_append (entry, g_node_new (
		   widget_info_new (_ ("Spin Buttons"), W_GTK_SPINBUTTON)));
	g_node_append (edit, g_node_new (
			   widget_info_new (_ ("Text boxes"), W_GTK_TEXT)));
	g_node_append (root, g_node_new (
			 widget_info_new (_ ("Event boxes"), W_GTK_EVENT)));
	g_node_append (root, item = g_node_new (
				widget_info_new (_ ("Items"), W_GTK_ITEM)));
	g_node_append (item, g_node_new (
			widget_info_new (_ ("ListItems"), W_GTK_LISTITEM)));
	g_node_append (item, g_node_new (
			widget_info_new (_ ("MenuItems"), W_GTK_MENUITEM)));
	g_node_append (item, g_node_new (
			widget_info_new (_ ("TreeItems"), W_GTK_TREEITEM)));
	g_node_append (root, g_node_new (
				widget_info_new (_ ("Lists"), W_GTK_LIST)));
	g_node_append (root, g_node_new (
				widget_info_new (_ ("Menus"), W_GTK_MENU)));
	g_node_append (root, g_node_new (
			  widget_info_new (_ ("Menubars"), W_GTK_MENUBAR)));
	g_node_append (root, notebook = g_node_new (
			widget_info_new (_ ("Notebooks"), W_GTK_NOTEBOOK)));
	g_node_append (notebook, g_node_new (
		    widget_info_new (_ ("Font Selectors"), W_GTK_FONTSEL)));
	g_node_append (root, g_node_new (
		 widget_info_new (_ ("Progress Bars"), W_GTK_PROGRESSBAR)));
	g_node_append (root, range = g_node_new (
			      widget_info_new (_ ("Ranges"), W_GTK_RANGE)));
	g_node_append (range, scale = g_node_new (
			      widget_info_new (_ ("Scales"), W_GTK_SCALE)));
	g_node_append (scale, g_node_new (
		  widget_info_new (_ ("Horizontal Scales"), W_GTK_HSCALE)));
	g_node_append (scale, g_node_new (
		    widget_info_new (_ ("Vertical Scales"), W_GTK_VSCALE)));
	g_node_append (range, scroll = g_node_new (
			 widget_info_new (_ ("Scrollbars"), W_GTK_SCROLL)));
	g_node_append (scroll, g_node_new (
	     widget_info_new (_ ("Horizontal Scrollbars"), W_GTK_HSCROLL)));
	g_node_append (scroll, g_node_new (
	       widget_info_new (_ ("Vertical Scrollbars"), W_GTK_VSCROLL)));
	g_node_append (root, g_node_new (
				widget_info_new (_ ("Trees"), W_GTK_TREE)));
	g_node_append (root, window = g_node_new (
			    widget_info_new (_ ("Windows"), W_GTK_WINDOW)));
	g_node_append (window, dialog = g_node_new (
			    widget_info_new (_ ("Dialogs"), W_GTK_DIALOG)));
	g_node_append (dialog, g_node_new (
		    widget_info_new (_ ("File Selectors"), W_GTK_FILESEL)));
	return root;
}

gboolean 
gnode2ctree (GtkCTree * ctree, guint depth, GNode * gnode,
	     GtkCTreeNode * cnode, gpointer data)
{
	WidgetInfo *info = gnode->data;

	gtk_ctree_set_node_info (ctree, cnode, info->name, 5, NULL, NULL,
			 NULL, NULL, (node->children) ? TRUE : FALSE, TRUE);
	gtk_object_set_user_data (GTK_OBJECT (cnode),
				  GUINT_TO_POINTER (info->type));
	g_free (info);
	return TRUE;
}

static void 
free_tree (GNode * node)
{
	g_return_if_fail (node != NULL);

	while (node->prev != NULL)
		node = node->prev;

	while (node != NULL)
	  {
		  if (node->data)
			  g_free (node->data);
		  if (node->children)
			  free_tree (node->children);
		  node = node->next;
	  }
}

GtkWidget *
widget_dialog_new ()
{
	GNode *tree = tree_new ();
	GtkWidget *ctree = gtk_ctree_new (1, 0);
	gpointer root;
	root = gtk_ctree_insert_gnode (GTK_CTREE (ctree), NULL, NULL, tree,
				gnode2ctree);
	gtk_object_set_user_data(GTK_OBJECT(ctree), root);
	/* Must free tree */
	free_tree (tree);
	g_node_destroy (tree);

	return ctree;
}
