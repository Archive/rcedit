#ifndef __RCSTYLE_IO_H__
#define __RCSTYLE_IO_H__

#include <gtk/gtk.h>
#include <config.h>
#include <stdio.h>


gchar *get_indent (guint indent);	/* Must be freed */

void print_style (GtkRcStyle * style, gchar * widget, gchar * engine,
		  FILE * file, guint indent);


#endif /* __RCSTYLE_IO_H__ */
