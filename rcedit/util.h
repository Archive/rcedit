
#ifndef __UTIL_H__
#define __UTIL_H__

#include <gnome.h>

gboolean file_exists (gchar * path, gchar * file);
gchar *find_pixmap_in_path (gchar ** pixmap_path, gchar * pixmap);	/* Must free */

#endif /* __UTIL_H__ */
